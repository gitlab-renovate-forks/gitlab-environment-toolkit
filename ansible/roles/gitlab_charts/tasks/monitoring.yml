- name: Setup Consul agent
  block:
    - name: Add Consul repo
      kubernetes.core.helm_repository:
        name: hashicorp
        repo_url: https://helm.releases.hashicorp.com

    - name: Update Consul repo
      command: helm repo update hashicorp

    - name: Get all Consul chart versions
      command: helm search repo hashicorp/consul -l -o json
      register: consul_charts_versions

    - name: Match Consul charts version to app version
      set_fact:
        consul_charts_version: "{{ (consul_charts_versions.stdout | from_json | selectattr('name', 'equalto', 'hashicorp/consul') | selectattr('app_version', 'equalto', consul_charts_app_version))[0].version | default('', true) }}"

    - name: Fail if Consul charts version can't be found
      fail:
        msg: "Consul charts version for {{ consul_charts_app_version }} could not be found. Exiting..."
      when: consul_charts_version == ''

    - name: Show Consul charts version
      debug:
        msg: "Consul charts version for {{ consul_charts_app_version }} is {{ consul_charts_version }}"

    - name: Install Consul chart
      kubernetes.core.helm:
        name: gitlab-consul
        chart_ref: hashicorp/consul
        chart_version: "{{ consul_charts_version }}"
        release_namespace: "{{ consul_charts_namespace }}"
        values:
          global:
            enabled: false
            datacenter: gitlab_consul
          client:
            enabled: true
            join: "{{ consul_int_addrs }}"
            nodeSelector: |
              workload: support
          connectInject:
            enabled: false

    - name: Configure internal Consul endpoint
      kubernetes.core.k8s:
        state: present
        definition:
          kind: Service
          metadata:
            name: gitlab-consul
            namespace: "{{ consul_charts_namespace }}"
          spec:
            type: ClusterIP
            ports:
              - port: 8500
                protocol: TCP
            selector:
              app: consul
              release: gitlab-consul
  when: "'consul' in groups"

- name: Setup kube-prometheus-stack chart
  block:
    - name: Add kube-prometheus-stack repo
      kubernetes.core.helm_repository:
        name: prometheus-community
        repo_url: https://prometheus-community.github.io/helm-charts

    - name: Update kube-prometheus-stack repo
      command: helm repo update prometheus-community

    - name: Get latest kube-prometheus-stack app version if configured
      block:
        - name: Get latest kube-prometheus-stack details
          command: helm search repo prometheus-community/kube-prometheus-stack -o json
          register: kube_prometheus_stack_charts_latest_app_version

        - name: Set latest kube-prometheus-stack app version if configured
          set_fact:
            kube_prometheus_stack_charts_app_version: "{{ (kube_prometheus_stack_charts_latest_app_version.stdout | from_json)[0].app_version }}"
      when: kube_prometheus_stack_charts_app_version == 'latest'

    - name: Get all kube-prometheus-stack versions
      command: helm search repo prometheus-community/kube-prometheus-stack -l -o json
      register: kube_prometheus_stack_charts_versions

    - name: Match kube-prometheus-stack charts version to app version
      set_fact:
        kube_prometheus_stack_charts_version: "{{ (kube_prometheus_stack_charts_versions.stdout | from_json | selectattr('name', 'equalto', 'prometheus-community/kube-prometheus-stack') | selectattr('app_version', 'equalto', kube_prometheus_stack_charts_app_version))[0].version | default('', true) }}"

    - name: Fail if kube-prometheus-stack charts version can't be found
      fail:
        msg: "kube-prometheus-stack charts version for {{ kube_prometheus_stack_charts_app_version }} could not be found. Exiting..."
      when: kube_prometheus_stack_charts_version == ''

    - name: Show kube-prometheus-stack charts version
      debug:
        msg: "kube-prometheus-stack charts version for {{ kube_prometheus_stack_charts_app_version }} is {{ kube_prometheus_stack_charts_version }}"

    - name: Get existing kube-prometheus-stack deployment details
      kubernetes.core.helm_info:
        name: gitlab-kube-prometheus-stack
        release_namespace: "{{ kube_prometheus_stack_charts_namespace }}"
      register: kube_prometheus_stack_charts_existing_info

    - name: Set existing kube-prometheus-stack app version
      set_fact:
        kube_prometheus_stack_charts_existing_app_version: "{{ kube_prometheus_stack_charts_existing_info.status.app_version }}"
      when: kube_prometheus_stack_charts_existing_info.status is defined

    - name: Update CRDs on existing kube-prometheus-stack deployment
      command: "kubectl apply --server-side --force-conflicts -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v{{ kube_prometheus_stack_charts_app_version | replace('v', '') }}/example/prometheus-operator-crd/monitoring.coreos.com_{{ item }}.yaml"
      loop:
        - alertmanagerconfigs
        - alertmanagers
        - podmonitors
        - probes
        - prometheusagents
        - prometheuses
        - prometheusrules
        - scrapeconfigs
        - servicemonitors
        - thanosrulers
      when:
        - kube_prometheus_stack_charts_existing_app_version is defined
        - kube_prometheus_stack_charts_app_version != kube_prometheus_stack_charts_existing_app_version

    - name: Delete old deprecated Node Exporter daemonset (<0.59.1)
      command: "kubectl delete daemonset -l app=prometheus-node-exporter --namespace {{ kube_prometheus_stack_charts_namespace }}"
      when:
        - kube_prometheus_stack_charts_existing_app_version is defined
        - (kube_prometheus_stack_charts_existing_app_version | replace('v', '')) is version('0.59.1', '<')
        - kube_prometheus_stack_charts_app_version != kube_prometheus_stack_charts_existing_app_version

    - name: Configure kube-prometheus-stack StorageClass for existing installs
      # Ensure same StorageClass is used if already bound to avoid config clashes
      # TODO: To be reviewed in 4.0.0 to be potentially removed as a breaking change
      block:
        - name: Get kube-prometheus-stack existing PVC information
          k8s_info:
            api_version: v1
            kind: PersistentVolumeClaim
            namespace: "{{ kube_prometheus_stack_charts_namespace }}"
            name: "prometheus-gitlab-kube-prometheus-sta-prometheus-db-prometheus-gitlab-kube-prometheus-sta-prometheus-0"
          register: kube_prometheus_stack_pvc_info

        - name: Set existing storage class
          set_fact:
            kube_prometheus_stack_charts_existing_storage_class: "{{ kube_prometheus_stack_pvc_info.resources[0].spec.storageClassName | default('') }}"

        - name: Show existing storage class
          debug:
            msg: "Existing StorageClass for kube-prometheus-stack is {{ kube_prometheus_stack_charts_existing_storage_class }}"
          when: gitlab_charts_show_values
      when:
        - cloud_provider != 'none'
        - kube_prometheus_stack_charts_existing_app_version is defined

    - name: Configure kube-prometheus-stack StorageClass for new installs if required (AWS EKS)
      # Set up new StorageClass if this is a new install and none has been configured
      # TODO: To be reviewed in 4.0.0 as the default for all installs as a breaking change
      block:
        - name: Configure kube-prometheus-stack StorageClass (AWS EKS)
          kubernetes.core.k8s:
            template: 'storage_classes/kube_prometheus_stack_gp3.aws.yml.j2'
            state: present

        - name: Set kube-prometheus-stack StorageClass (AWS EKS)
          set_fact:
            kube_prometheus_stack_charts_storage_class: "gitlab-kube-prometheus-stack-gp3"
      when:
        - cloud_provider == 'aws'
        - kube_prometheus_stack_charts_existing_app_version is not defined
        - kube_prometheus_stack_charts_storage_class == ''

    - name: Configure default kube-prometheus-stack StorageClass for new installs if required (GCP GKE)
      # Set up new StorageClass if this is a new install and none has been configured
      # TODO: To be reviewed in 4.0.0 as the default for all installs as a breaking change
      block:
        - name: Configure default kube-prometheus-stack StorageClass (GCP GKE)
          kubernetes.core.k8s:
            template: 'storage_classes/kube_prometheus_stack_balanced.gcp.yml.j2'
            state: present

        - name: Set default kube-prometheus-stack StorageClass (GCP GKE)
          set_fact:
            kube_prometheus_stack_charts_storage_class: "gitlab-kube-prometheus-stack-balanced"
      when:
        - cloud_provider == 'gcp'
        - kube_prometheus_stack_charts_existing_app_version is not defined
        - kube_prometheus_stack_charts_storage_class == ''

    - name: Lookup kube-prometheus-stack chart values values
      set_fact:
        kube_prometheus_stack_charts_values: "{{ lookup('template', 'templates/kube-prometheus-stack.yml.j2') | from_yaml }}"

    - name: Check if custom kube-prometheus-stack chart values are provided
      stat:
        path: "{{ kube_prometheus_stack_charts_custom_config_file }}"
      register: kube_custom_config_file

    - name: Merge in custom kube-prometheus-stack chart values if provided
      set_fact:
        kube_prometheus_stack_charts_values: "{{ kube_prometheus_stack_charts_values | combine(lookup('template', kube_prometheus_stack_charts_custom_config_file) | from_yaml, recursive=True) }}"
      when: kube_custom_config_file.stat.exists

    - name: Show charts values if configured
      debug:
        msg: "{{ kube_prometheus_stack_charts_values }}"
      when: gitlab_charts_show_values

    - name: Install kube-prometheus-stack chart
      kubernetes.core.helm:
        name: gitlab-kube-prometheus-stack
        chart_ref: prometheus-community/kube-prometheus-stack
        chart_version: "{{ kube_prometheus_stack_charts_version }}"
        release_namespace: "{{ kube_prometheus_stack_charts_namespace }}"
        values: "{{ kube_prometheus_stack_charts_values }}"
      register: kube_prometheus_stack_charts_deploy
      retries: 3
      delay: 3
      until: kube_prometheus_stack_charts_deploy is success

- name: Redeploy GitLab Charts to configure monitoring ServiceMonitors if first deploy
  block:
    - name: Wait until monitoring required cluster API is available
      kubernetes.core.k8s_cluster_info:
      register: cluster_api_status
      until: cluster_api_status.apis['monitoring.coreos.com/v1'] is defined
      retries: 12
      delay: 5

    - name: Redeploy GitLab Charts to configure monitoring ServiceMonitors if first deploy
      kubernetes.core.helm:
        name: gitlab
        chart_ref: "{{ gitlab_charts_ref }}"
        chart_version: "{{ gitlab_charts_version | default(None) }}"
        release_namespace: "{{ gitlab_cloud_native_release_namespace }}"
        values: "{{ gitlab_charts_values }}"
      when: not gitlab_operator
  when:
    - kube_prometheus_stack_charts_existing_app_version is not defined
